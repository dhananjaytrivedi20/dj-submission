package com.example.kotlincoroutines.repo

import android.util.Log
import com.example.kotlincoroutines.api.UserApiInterface
import com.example.kotlincoroutines.model.UserApiResponseModel
import javax.inject.Inject


class MainRepo @Inject constructor(val userApi: UserApiInterface) {

    suspend fun getUserData(page: Int): UserApiResponseModel? {

        return try {
            val response = userApi.getUsers(page)
            response
        } catch (e: Exception) {
            Log.d("DEEJAY", "Exception here ${e.message}")
            return null
        }
    }

}

