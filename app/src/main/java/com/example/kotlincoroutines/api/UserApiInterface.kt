package com.example.kotlincoroutines.api

import com.example.kotlincoroutines.model.UserApiResponseModel
import kotlinx.coroutines.delay
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApiInterface {

    companion object {
        const val BASE_URL: String = "https://reqres.in/api/"
    }

    @GET("users")
    suspend fun getUsers(
        @Query("page") page: Int
    ): UserApiResponseModel

}