package com.example.kotlincoroutines.ui.main

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlincoroutines.R
import com.example.kotlincoroutines.model.User
import com.example.kotlincoroutines.ui.UserListAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var textview: TextView
    private lateinit var button: Button
    private lateinit var recyclerview: RecyclerView
    private lateinit var adapter: UserListAdapter
    private val viewModel: MainActivityViewModel by viewModels()
    private val listOfUsers = mutableListOf<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // DO data binding to avoid this stupidity
        textview = findViewById(R.id.textview)
        button = findViewById(R.id.button)

        initReyclerView()

        button.setOnClickListener {
            Log.d("DEEJAY", "Clicked")
            viewModel.getUserData()
        }

        viewModel.listOfUsers.observe(this, {
            this.listOfUsers.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun initReyclerView() {
        recyclerview = findViewById<RecyclerView>(R.id.recycler_view)

        // this creates a vertical layout Manager
        recyclerview.layoutManager = GridLayoutManager(this, 2)

        // This will pass the ArrayList to our Adapter
        adapter = UserListAdapter(listOfUsers)
        // Setting the Adapter with the recyclerview
        recyclerview.adapter = adapter

        recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0) {
                    val layoutManager = recyclerview.layoutManager as GridLayoutManager
                    val visibleItemCount = layoutManager.findLastCompletelyVisibleItemPosition() + 1
                    if (visibleItemCount == layoutManager.itemCount) {
                        viewModel.getUserData()
                    }

                }
            }
        })

    }
}