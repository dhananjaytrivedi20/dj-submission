package com.example.kotlincoroutines.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.kotlincoroutines.model.User
import com.example.kotlincoroutines.repo.MainRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(private val mainRepo: MainRepo) : ViewModel() {

    val listOfUsers = MutableLiveData<List<User>>()
    var initialPage = 0

    fun getUserData() {

        initialPage++

        viewModelScope.launch {
            val response = mainRepo.getUserData(initialPage)
            listOfUsers.value = response?.data


        }
    }

}