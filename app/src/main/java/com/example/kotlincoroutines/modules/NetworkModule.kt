package com.example.kotlincoroutines.modules

import com.example.kotlincoroutines.api.UserApiInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofitApi() : Retrofit = Retrofit.Builder().baseUrl(UserApiInterface.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit) : UserApiInterface {
        return retrofit.create(UserApiInterface::class.java)
    }
}